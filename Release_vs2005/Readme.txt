TXDViewer v2 Manual

Note: if can't run the TXDViewer.exe, please install vcredist.exe first.

1. In 'Pan' Mode, Mouse left button down to move map

2. In 'Identify' Mode, 
   Mouse left button click to select one feature, 
   Mouse right button click to draw a rectangle, select features in the rectangle.

3. Mouse wheel to zoom in and zoom Out

4. Now support TXD, Shapefile and PostGIS (File -> Connect DB)

5. Show Feature's ID

6. Copy and Export TXD records. (Mouse right click in Identify Window)

Shortcut Keys
1. Ctrl + F       Find a feature by ID
2. Ctrl + L		  Locate to a coordinates
3. Ctrl + D       Pan Mode
4. Ctrl + E       Identify Mode
5. ESC            Close sub window, such as Locate Window and Identify Window

---------------------------------------华丽的分割线-------------------------------------------

主要功能

1. 支持TXD，SHP文件和PostGIS的不完全支持
2. 支持drag & drop
3. ‘Pan’ 模式下，按住鼠标左键拖动地图
4. ‘Identify’ 模式下（感叹号图标），点击鼠标左键选择单个feature，或者按鼠标右键框选多个feature。
    按住Ctrl键可多选。
    按住鼠标左键仍可拖动地图。
5. 鼠标滚轮放大、缩小地图。
6. 显示feature ID（ID图标）
7. 复制和导出选择的TXD（Identify窗口鼠标右键）
6. 后台加载数据，主界面不会卡死。

快捷键
1. Ctrl + F       通过ID搜索feature
2. Ctrl + L		  通过坐标定位
3. Ctrl + D       Pan Mode
4. Ctrl + E       Identify Mode
5. ESC            关闭子窗口，比如搜索窗口，属性窗口。