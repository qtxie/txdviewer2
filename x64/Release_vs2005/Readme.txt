TXDViewer v2 Manual

Note: if can't run the TXDViewer.exe, please install vcredist.exe first.

1. In 'Pan' Mode, Mouse left button down to move map

2. In 'Identify' Mode, 
   Mouse left button click to select one feature, 
   Mouse right button click to draw a rectangle, select features in the rectangle.

3. Mouse wheel to zoom in and zoom Out

4. Now support TXD, Shapefile and PostGIS (File -> Connect DB)

5. Show Feature's ID

6. Copy and Export TXD records. (Mouse right click in Identify Window)

Shortcut Keys
1. Ctrl + F       Find a feature by ID
2. Ctrl + L		  Locate to a coordinates
3. Ctrl + D       Pan Mode
4. Ctrl + E       Identify Mode
5. ESC            Close sub window, such as Locate Window and Identify Window

---------------------------------------华丽的分割线-------------------------------------------

主要功能

1. 支持TXD，SHP文件和PostGIS数据库
2. 支持drag & drop, 支持拖拽多个文件
3. ‘Pan’ 模式下，按住鼠标左键拖动地图
4. ‘Identify’ 模式下（感叹号图标）
    - 点击鼠标`左键`选择单个feature
    - 鼠标`右键`框选多个feature。
    - 按住Ctrl键可多选。
    - 按住Ctrl键，重复选择已经选中的feature，可取消选择。
    - 鼠标左键仍可拖动地图。
5. 鼠标滚轮放大、缩小地图。
6. 显示feature ID（ID图标）
7. 复制和导出选择的TXD（Identify窗口鼠标右键）
6. 后台加载数据，主界面不会卡死。

关于SQL查询；
1. 简单SQL语句（不需要join多张表），不需要填写完整的 select 语句，将会作用在选择的所有table上面。
   比如：
   填写的SQL语句为：WHERE ST_Within(the_geom, GeometryFromText('POLYGON((75 20,80 30,90 22,85 10,75 20))', 4326)
   选择的表：       table1， table2
   结果：           table1 和 table2 中在POLYGON((75 20,80 30,90 22,85 10,75 20)里的记录会被选出来。
2. 复杂的SQL语句（需要join多张表），这时需要填写完整的 select 语句，
   必须保证前面两个column是 id 和 geometry， geometry 需要 encode 为十六进制的WKB。
   SELECT id, encode(ST_AsBinary(ST_Force_2D(geom)), 'hex') from table_name where ...)

快捷键
1. Ctrl + F       通过ID搜索feature
2. Ctrl + L		  通过坐标定位
3. Ctrl + D       Pan Mode
4. Ctrl + E       Identify Mode
5. Ctrl + S       导出选择的TXD records
6. ESC            关闭子窗口，比如搜索窗口，属性窗口。